using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath((new DirectoryInfo("../Otus.Teaching.PromoCodeFactory.WebHost")).FullName)
                .AddJsonFile("appsettings.Development.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DataContext>();
            var connectionString = configuration.GetConnectionString("PGSQLConnectionString");
            builder.UseNpgsql(connectionString);

            return new DataContext(builder.Options);

        }
    }
}